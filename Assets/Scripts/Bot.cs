﻿using UnityEngine;
using Agario.AI;

namespace Agario
{
    public class Bot : Controller
    {
        private Context _context;
        private CircleCollider2D _seekingField;

        public override void Awake()
        {
            base.Awake();
            _context = new Context(this);
            _seekingField = GetComponent<CircleCollider2D>();
        }

        public override void Update()
        {
            base.Update();
            if (!_dead)
            {
                _context.Process();
                Scan();
            }
        }

        private void Scan() //the ugliest decision
        {
            if (_seekingField.radius < BiggestAvatar.Radius * 2f)//can be customized
                _seekingField.radius += 0.25f;
            else _seekingField.radius = 0f;
        }

        public void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Consumable"))
            {
                if (other.transform.parent == transform.parent)
                    return;

                var enemy = other.GetComponent<CircleUnit>();
                if (enemy != null)
                    _context.Analyze(enemy);
            }
        }
    }
}