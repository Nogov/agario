﻿using UnityEngine;

namespace Agario
{
    public abstract class CircleUnit : MonoBehaviour
    {
        [SerializeField]
        private float _baseSize;
        protected new Transform transform;
        public float Radius
        {
            get { return transform.localScale.x; ; }
            set { transform.localScale = Vector3.one * value; }
        }
        public float Square
        {
            get { return Mathf.PI * Radius * Radius; }
            set { transform.localScale = Vector3.one * Mathf.Sqrt(value / Mathf.PI); }
        }

        public void Awake()
        {
            transform = GetComponent<Transform>();
        }

        public virtual void Arise()
        {
            transform.localScale = Vector3.one * _baseSize;
        }

        public virtual void Die() { }
    }
}