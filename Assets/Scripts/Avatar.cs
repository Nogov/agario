﻿using UnityEngine;

namespace Agario
{
    public class Avatar : CircleUnit
    {
        private const float MIN_RADIUS = 1f;
        private const float SPLIT_SIZE = 2f;

        protected Vector2 _destination;
        public Vector2 Destination
        {
            get { return _destination; }
            set { _destination = value; }
        }

        [SerializeField]
        private float _speedDecreaseRate;
        public float Speed { get { return 1f / (_speedDecreaseRate * Mathf.Sqrt(Radius)); } }

        public void Consume(CircleUnit victim)
        {
            var transfer = victim.Square;
            Square += transfer;
            victim.Square -= transfer;
            if (victim.Radius < MIN_RADIUS)
                victim.Die();
        }

        public void Split(Vector2 direction)
        {
            if (Radius < SPLIT_SIZE)
                return;

            var offset = (Vector3)direction * Radius;

            var clone = Instantiate(gameObject, transform.position + offset, Quaternion.identity) as GameObject;
            clone.name = "Avatar";
            clone.transform.SetParent(transform.parent);

            var cloneAvatar = clone.GetComponent<Avatar>();
            var relativeForce = direction * Square;

            Square = cloneAvatar.Square /= 2;

            var cloneBody = cloneAvatar.GetComponent<Rigidbody2D>();
            cloneBody.AddRelativeForce(relativeForce, ForceMode2D.Impulse);
        }

        //This is weird but I need to use both 
        public void OnCollisionStay2D(Collision2D collision)
        {
            ProcessCollider(collision.collider);            
        }

        public void OnCollisionEnter2D(Collision2D collision)
        {
            ProcessCollider(collision.collider);           
        }

        private void ProcessCollider(Collider2D collider)
        {
            if (collider.CompareTag("Consumable"))
            {
                var avatar = collider.GetComponent<CircleUnit>();
                if (Radius > avatar.Radius)
                    Consume(avatar);
            }
        }

        public void Update()
        {
            transform.localPosition = Vector2.Lerp(transform.localPosition, Vector3.zero, Time.deltaTime * 5f * Speed);
        }

        public override void Die()
        {
            Destroy(gameObject);
        }
    }
}