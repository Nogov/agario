﻿using UnityEngine;

namespace Agario
{
    public class SmoothFollow : MonoBehaviour
    {
        private Player _target;
        public Player Target { get { return _target; } set { _target = value; } }

        [SerializeField]
        private float _distance;
        [SerializeField]
        private float _offset;

        private Vector2 _sizeRange;
        private Camera _self;
        [SerializeField]
        private float _scaleCft;

        void Start()
        {
            _self = GetComponent<Camera>();
        }

        void LateUpdate()
        {
            var newPos = _target.transform.position + Vector3.right * _offset - Vector3.forward * _distance;
            //newPos.x = Mathf.Clamp(newPos.x, -35f, 35f);//change to
            //newPos.y = Mathf.Clamp(newPos.y, -40f, 40f);//non-constant
            transform.position = Vector3.Lerp(transform.position, newPos, Time.fixedDeltaTime * 2f);

            if (_target.BiggestAvatar != null)
                _self.orthographicSize = Mathf.Lerp(_self.orthographicSize,
                    Mathf.Clamp(_scaleCft * _target.BiggestAvatar.Radius, 4, 8),
                    Time.fixedDeltaTime);
        }
    }
}