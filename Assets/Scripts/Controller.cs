﻿using UnityEngine;

namespace Agario
{
    public abstract class Controller : MonoBehaviour
    {
        [SerializeField]
        private GameObject _baseAvatar;

        protected new Transform transform;

        protected Vector2 _destination;
        public Vector2 Destination
        {
            get { return _destination; }
            set { _destination = value; }
        }

        public Avatar BiggestAvatar
        {
            get
            {
                if (_avatars.Length == 0) return null;
                var biggest = _avatars[0];
                for (int i = 1; i < _avatars.Length; i++)
                {
                    if (biggest.Radius < _avatars[i].Radius)
                        biggest = _avatars[i];
                }
                return biggest;
            }
        }
        protected Avatar[] _avatars { get { return GetComponentsInChildren<Avatar>(); } }

        protected bool _dead;

        [SerializeField]
        protected float _respawnTime;
        protected float _timeSpentDead;

        public virtual void Awake()
        {
            transform = GetComponent<Transform>();
            Reset();
        }

        public virtual void Start() { }

        public virtual void Update()
        {
            _dead = _avatars.Length == 0;
            if (_dead)
            {
                _timeSpentDead += Time.deltaTime;
                if (_timeSpentDead >= _respawnTime)
                    Respawn();
            }
            else transform.position = Vector2.MoveTowards(transform.position, _destination, BiggestAvatar.Speed);
        }

        public void Reset()
        {
            var go = Instantiate(_baseAvatar, transform.position, Quaternion.identity) as GameObject;
            go.transform.SetParent(transform);

            _destination = transform.position;
        }

        public void Split()
        {
            var _currentAvatars = _avatars;     //↓ why the length isn't fixed?
            for (var i = 0; i < _currentAvatars.Length; i++)
            {
                var direction = _destination - (Vector2)_avatars[i].transform.position;
                direction.Normalize();
                _avatars[i].Split(direction);
            }
        }

        public virtual void Respawn()
        {
            _timeSpentDead = 0;
            transform.position = Helpers.RandomVector2;
            Reset();
        }
    }
}