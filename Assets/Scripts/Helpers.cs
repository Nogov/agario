﻿using UnityEngine;

namespace Agario
{
    public static class Helpers
    {
        public static Vector2 RandomVector2
        {
            //TODO: Add level bounds
            get { return new Vector2(Random.Range(-49f, 49f), Random.Range(-49f, 49f)); }
        }
        public static Color RandomColor
        {
            get { return new Color(Random.Range(0, 256), Random.Range(0, 256), Random.Range(0, 256)); }
        }
    }
}