﻿using UnityEngine;

namespace Agario.InputManagement
{
    public static class DeviceManager
    {
        public static IDeviceInputManager GetInputManager(RuntimePlatform platform)
        {
            switch (platform)
            {
                case RuntimePlatform.Android:
                case RuntimePlatform.IPhonePlayer:
                    return new Touch();
                default: return new Desktop();
            }
        }
    }
}
