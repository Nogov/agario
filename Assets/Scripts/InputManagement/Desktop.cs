﻿using UnityEngine;

namespace Agario.InputManagement
{
    public class Desktop : IDeviceInputManager
    {
        private Vector3 _direction;
        public Vector3 Direction { get { return _direction; } }
        public bool Split { get { return Input.GetButtonDown("Split"); } }
        public bool Exit { get { return Input.GetKey(KeyCode.Escape); } }

        public void Update()
        {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            var raycasthit = Physics2D.Raycast(ray.origin, ray.direction);
            if (raycasthit)
                _direction = raycasthit.point;
        }
    }
}
