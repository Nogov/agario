﻿using UnityEngine;

namespace Agario.InputManagement
{
    public interface IDeviceInputManager
    {
        Vector3 Direction { get; }
        bool Split { get; }
        bool Exit { get; }
        void Update();
    }
}