﻿using UnityEngine;

namespace Agario.InputManagement
{
    public class Touch : IDeviceInputManager
    {
        private Vector3 _direction;
        public Vector3 Direction { get { return _direction; } }
        public bool Split { get { return Input.touchCount >= 2; } }
        public bool Exit { get { return Input.GetKey(KeyCode.Escape); } }

        public void Update()
        {
            foreach (var t in Input.touches)
            {
                var ray = Camera.main.ScreenPointToRay(t.position);
                var raycasthit = Physics2D.Raycast(ray.origin, ray.direction);
                if (raycasthit && (t.phase == TouchPhase.Moved || t.phase == TouchPhase.Stationary))
                    _direction = raycasthit.point;
            }
        }
    }
}