﻿using Agario.InputManagement;
using UnityEngine;

namespace Agario
{
    public class Player : Controller
    {
        private IDeviceInputManager _inputManager;

        public override void Awake()
        {
            base.Awake();
            _inputManager = DeviceManager.GetInputManager(Application.platform);
        }

        public override void Start()
        {
            Camera.main.GetComponent<SmoothFollow>().Target = this;
        }

        public override void Update()
        {
            base.Update();
            _inputManager.Update();

            if (!_dead)
            {
                _destination = _inputManager.Direction;
                if (_inputManager.Split)
                    Split();
            }

            if (_inputManager.Exit)
                Application.Quit();
        }
    }
}