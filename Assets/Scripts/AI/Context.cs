﻿namespace Agario.AI
{
    public class Context
    {
        private IStrategy _strategy;
        private Bot _bot;
        private CircleUnit _currentEnemy;

        public Context(Bot bot)
        {
            _bot = bot;
            _strategy = new Chilling();
        }

        public void Analyze(CircleUnit newEnemy)
        {
            if (_bot.BiggestAvatar == null)
                return;
            if (_currentEnemy != null && newEnemy.Radius <= _currentEnemy.Radius)
                return;//keeping strategy

            if (_bot.BiggestAvatar.Radius < newEnemy.Radius)
                _strategy = new Escaping(newEnemy);
            else if (_bot.BiggestAvatar.Radius > newEnemy.Radius)
                _strategy = new Chasing(newEnemy);

            _currentEnemy = newEnemy;
        }

        public void Process()
        {
            if (!_strategy.Behavior(_bot))
            {
                _currentEnemy = null;
                _strategy = new Chilling();
            }
        }
    }
}
