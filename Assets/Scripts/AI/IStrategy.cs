﻿namespace Agario.AI
{
    public interface IStrategy
    {
        /// <summary>
        /// Behavior of the current Bot
        /// </summary>
        /// <param name="bot">Bot player</param>
        /// <returns>true means to keep current behavior; false means the behavior has to be changed</returns>
        bool Behavior(Bot bot);
    }
}
