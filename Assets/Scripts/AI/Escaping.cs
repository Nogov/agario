﻿using UnityEngine;

namespace Agario.AI
{
    public class Escaping : IStrategy
    {
        private float _safetyRange = 5f;
        private CircleUnit _chaser;

        public Escaping(CircleUnit chaser)
        {
            _chaser = chaser;
        }

        public bool Behavior(Bot bot)
        {
            if (_chaser == null)
                return false;
            if (bot.BiggestAvatar.Radius >= _chaser.Radius)
                return false;
            if ((_chaser.transform.position - bot.transform.position).sqrMagnitude
                - (_chaser.Radius + bot.BiggestAvatar.Radius) > _safetyRange)
                return false;

            var direction = _chaser.transform.position - bot.transform.position;
            Vector3 noise = Helpers.RandomVector2;

            bot.Destination = bot.transform.position - direction.normalized + noise;

            return true;
        }
    }
}