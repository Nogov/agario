﻿using UnityEngine;

namespace Agario.AI
{
    public class Chasing : IStrategy
    {
        private CircleUnit _victim;
        private float _greedinessRange = 5f;

        public Chasing(CircleUnit victim)
        {
            _victim = victim;
        }

        public bool Behavior(Bot bot)
        {
            if (_victim == null)
                return false;
            if ((_victim.transform.position - bot.transform.position).sqrMagnitude
                - (_victim.Radius + bot.BiggestAvatar.Radius) > _greedinessRange)
                return false;
            if (bot.BiggestAvatar.Radius <= _victim.Radius)
                return false;

            bot.Destination = _victim.transform.position;

            //if size in (1/5, 1/2) of bot
            if (bot.BiggestAvatar.Square / 2 > _victim.Square
                && bot.BiggestAvatar.Square * 0.2f < _victim.Square)
                bot.Split();

            return true;
        }
    }
}