﻿using UnityEngine;

namespace Agario.AI
{
    public class Chilling : IStrategy
    {
        private float _closeEnough = 1f;

        public bool Behavior(Bot bot)
        {
            if (((Vector2)bot.transform.position - bot.Destination).sqrMagnitude < _closeEnough)
                bot.Destination = Helpers.RandomVector2;
            return false;
        }
    }
}