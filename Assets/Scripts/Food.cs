﻿namespace Agario
{
    public class Food : CircleUnit
    {
        public override void Die()
        {
            Arise();
        }

        public override void Arise()
        {
            transform.position = Helpers.RandomVector2;
            base.Arise();
        }
    }
}