﻿using UnityEngine;

namespace Agario
{
    public class Initializer : MonoBehaviour
    {
        [SerializeField]
        private GameObject _foodPrefab;
        [SerializeField]
        private GameObject _botPrefab;

        [SerializeField]
        private float _minimumBotSize;

        [SerializeField]
        private bool _prewarm;
        [SerializeField]
        private int _botsCount;
        [SerializeField]
        private int _foodCount;

        public void Awake()
        {
            CreateFoodPool();
            SpawnBots();
        }

        private void CreateFoodPool()
        {
            var foodParent = new GameObject("Food");
            foodParent.transform.SetParent(transform);

            for (var i = 0; i < _foodCount; i++)
            {
                var go = Instantiate(_foodPrefab, Helpers.RandomVector2, Quaternion.identity) as GameObject;
                go.transform.SetParent(foodParent.transform);
            }
        }

        private void SpawnBots()
        {
            var botsParent = new GameObject("Bots");
            botsParent.transform.SetParent(transform);

            for (var i = 0f; i < _botsCount; i++)
            {
                var go = Instantiate(_botPrefab, Helpers.RandomVector2, Quaternion.identity) as GameObject;
                if (_prewarm)
                {
                    //Slightly modified Zipf's Law
                    var zipfySize = 5f / (0.2f * i + 1);
                    var avatar = go.GetComponentInChildren<Avatar>();
                    avatar.transform.localScale *= (zipfySize > _minimumBotSize) ? zipfySize : _minimumBotSize;
                }
                go.transform.SetParent(botsParent.transform);
            }
        }
    }
}