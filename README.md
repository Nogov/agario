Примененные подходы:
1. ИИ работает на основе паттерна Strategy
2. Нейтральные единицы Food неявно реализуют Object Pool
3. Флаг Prewarm в Initializer.cs позволяет распределить размеры ботов по закону Ципфа, что придает игре ощущения с реальными людьми
4. bot.Scan() моделирует работу радара, что позволяет с одной стороны более точно отлавливать столкновения, а с другой лишает ботов излишней нестественности поведения

Проблемные моменты:
1. Нет привязки к размерам игрового мира (у камеры и вспомогательного метода Helpers.RandomVector2)
2. Параметр noise в алгоритме Escaping вызывает подергивание бота
3. Логика поведения ботов привязана параметром наибольшего аватара
4. Объекты Food не работают без компонента Rigidbody2D